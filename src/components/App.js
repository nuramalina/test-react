import React, { Component } from "react";
import Checkbox from "./Checkbox";

const OPTIONS = ["One", "Two", "Three"];
const questions = [1,2,3,4,5];

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checkboxes: OPTIONS.reduce(
        (options, option) => ({
          ...options,
          [option]: false
        }),
        {}
      ),
      answer:[],
      currentQuestion :0
    };

    // This binding is necessary to make `this` work in the callback
    this.handlePrevClick = this.handlePrevClick.bind(this);
  }
  
  createCheckbox = option => (
    <Checkbox
      label={option}
      isSelected={this.state.checkboxes[option]}
      onCheckboxChange={this.handleCheckboxChange}
      key={option}
    />
  );

  createCheckboxes = () => OPTIONS.map(this.createCheckbox);

  selectAllCheckboxes = isSelected => {
    Object.keys(this.state.checkboxes).forEach(checkbox => {
      // BONUS: Can you explain why we pass updater function to setState instead of an object?
      this.setState(prevState => ({
        checkboxes: {
          ...prevState.checkboxes,
          [checkbox]: isSelected
        }
      }));
    });
  };

  selectAll = () => this.selectAllCheckboxes(true);

  deselectAll = () => this.selectAllCheckboxes(false);

  handleCheckboxChange  = changeEvent => {
    const { name } = changeEvent.target;
    
      this.state.answer[this.state.currentQuestion]= {
        ...this.state.answer[this.state.currentQuestion], name
      };

    this.setState(prevState => ({
      checkboxes: {
        ...prevState.checkboxes,
        [name]: !prevState.checkboxes[name]
      }
    }));
  };

  handleFormSubmit = formSubmitEvent => {
    formSubmitEvent.preventDefault();

    this.setState({ currentQuestion: this.state.currentQuestion + 1 })

    Object.keys(this.state.checkboxes)
      .filter(checkbox => this.state.checkboxes[checkbox])
      .forEach(checkbox => {
        console.log(checkbox, "is selected.");
        console.log(this.state.currentQuestion);
        console.log(this.state.answer);
      });
      console.log(this.state.answer.hasOwnProperty(this.state.currentQuestion));
    
    if(this.state.answer.hasOwnProperty(this.state.currentQuestion)){
      this.deselectAll();
    }
  };

  handlePrevClick(e) {
    e.preventDefault();
    this.setState({ currentQuestion: this.state.currentQuestion - 1 });
    
  };

  

  render() {
    return (
      <div className="container">
        <div className="row mt-5">
          <div className="col-sm-12">
            <div className='question-section'>
                <span>Question {questions[this.state.currentQuestion]}</span>
            </div>
            <form onSubmit={this.handleFormSubmit}>
              {this.createCheckboxes()}

              <div className="form-group mt-2">
                  {this.state.currentQuestion +1 !== 1 &&
                   <button type="button" className="btn btn-primary" onClick={this.handlePrevClick} >
                        Prev
                      </button>
                  }
                  {this.state.currentQuestion +1 !== 5 ?(
                      <button type="submit" className="btn btn-primary" >
                        Next
                      </button>
                    ) :(
                      <button type="submit" className="btn btn-primary" >
                        Finish
                      </button>
                    )
                  }
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
