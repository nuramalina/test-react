import React from "react";

class Tombol extends React.Component {
 render() {
         
         return (
             <div>
             	{this.props.dataSoal !== 1 &&
			        <button type="button" className="btn btn-primary">
	                  Prev
	                </button>
			      }
                
                <button type="submit" className="btn btn-primary" style={this.props.hasMargin ? ({ marginLeft: '0.8rem' }) : ({})}>
                  Next
                </button>
             </div>
         );
     }
 }

 export default Tombol;